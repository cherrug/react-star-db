import React from 'react';

import './ErrorIndicator.css';

import pic from './death-star.png';

const ErrorIndicator = () => (
    <div className="error-indicator">
        <img src={pic} alt="death_star" />
        <span>OOPS.. something went wrong</span>
    </div>
);

export default ErrorIndicator;
