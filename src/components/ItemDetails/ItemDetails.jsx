import React from 'react';

import './ItemDetails.css';
import DetailsList from '../DetailsList';

const ItemDetails = ({ data }) => (
        <div className="item-details">
            <div className="row">
                <div className="col-4">
                    <img
                        className="item-details__img"
                        src={data.img}
                        alt="item pic"
                    />
                </div>
                <div className="col-8">
                    <h4>{data.name}</h4>
                    <DetailsList list={data.details} />
                </div>
            </div>
        </div>
    );

export default ItemDetails;
