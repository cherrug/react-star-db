import { withData } from '../HOC';
import ItemList from '../ItemList';
import SwapiService from '../../services/SwapiService';

const swapi = new SwapiService();

const { getAllPeople, getAllPlanets, getAllStarships } = swapi;

const PeopleList = withData(ItemList, getAllPeople);
const PlanetList = withData(ItemList, getAllPlanets);
const StarshipsList = withData(ItemList, getAllStarships);

export { PeopleList, PlanetList, StarshipsList };
