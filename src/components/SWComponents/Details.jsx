import { withData } from '../HOC';
import SwapiService from '../../services/SwapiService';
import ItemDetails from '../ItemDetails';

const swapi = new SwapiService();

const { getPerson, getPlanet, getStarship } = swapi;

const PersonDetails = withData(ItemDetails, getPerson);

const PlanetDetails = withData(ItemDetails, getPlanet);
const StarshipDetails = withData(ItemDetails, getStarship);

export { PlanetDetails, PersonDetails, StarshipDetails };
