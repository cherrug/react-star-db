import { PeopleList, PlanetList, StarshipsList } from './Lists';

import { PlanetDetails, PersonDetails, StarshipDetails } from './Details';

export {
    PlanetDetails,
    PersonDetails,
    StarshipDetails,
    PeopleList,
    PlanetList,
    StarshipsList,
};
