import React from 'react';
import { capitalize } from '../../services/StringUtils';

const Header = ({ links, onLinkSelected }) => {
    const lis = links.map((link, idx) => (
        <li className="nav-item" key={idx} onClick={() => onLinkSelected(link)}>
            <a className="nav-link" href={`/${link}`}>
                {capitalize(link)}
            </a>
        </li>
    ));

    return (
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
            <a className="navbar-brand" href="/">
                Star Wars DB
            </a>

            <div className="collapse navbar-collapse">
                <ul className="navbar-nav m-auto">{lis}</ul>
            </div>
        </nav>
    );
};
export default Header;
