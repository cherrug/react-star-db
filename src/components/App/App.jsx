import React from 'react';

import './App.css';
import Header from '../Header';
import RandomPlanet from '../RandomPlanet';
import ItemDetails from '../ItemDetails';
import Content from '../Content';
import SwapiService from '../../services/SwapiService';
import {
    PeopleList,
    PlanetList,
    StarshipsList,
    PlanetDetails,
    PersonDetails,
    StarshipDetails,
} from '../SWComponents';

export default class App extends React.Component {
    links = ['people', 'planets', 'starships'];
    swapi = new SwapiService();

    state = {
        link: 'planets',
        selected: null,
    };

    onItemSelected = (item) => {
        this.setState({
            selected: item,
        });
    };

    onLinkSelected = (link) => {
        this.setState({
            link,
        });
    };

    render() {
        const { selected } = this.state;

        const peopleList = (
            <PeopleList label="people" onItemSelected={this.onItemSelected} />
        );

        const planetList = (
            <PlanetList label="planets" onItemSelected={this.onItemSelected} />
        );

        const starshipsList = (
            <StarshipsList
                label="starships"
                onItemSelected={this.onItemSelected}
            />
        );

        const itemDetails = <ItemDetails item={selected} />;

        return (
            <>
                <Header
                    links={this.links}
                    onLinkSelected={this.onLinkSelected}
                />
                <div className="container">
                    <RandomPlanet />
                    <PersonDetails itemId={4} />
                    <PlanetDetails itemId={10} />
                    {/* <Content left={peopleList} right={personDetails} />
                    <Content left={planetList} right={starshipsList} /> */}
                </div>
            </>
        );
    }
}
