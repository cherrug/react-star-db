import React from 'react';

import './ItemList.css';
import { capitalize } from '../../services/StringUtils';
const ItemList = (props) => {
    const { data, onItemSelected, label } = props;

    const items = data.map((item) => (
        <li
            className="list-group-item"
            key={item.id}
            onClick={() => onItemSelected(item)}
        >
            {item.name}
        </li>
    ));
    return (
        <>
            <h4>{capitalize(label)}</h4>
            <ul className="list-group">{items}</ul>
        </>
    );
};

export default ItemList;
