import React from 'react';

import SwapiService from '../../services/SwapiService';
import DetailsList from '../DetailsList';
import Spinner from '../Spinner';
import './RandomPlanet.css';
import ErrorIndicator from '../ErrorIndicator/ErrorIndicator';

export default class RandomPlanet extends React.Component {
    swapiService = new SwapiService();

    state = {
        loaded: false,
        error: false,
        planet: {},
    };

    componentDidMount() {
        this.updatePlanet();
        this.interval = setInterval(this.updatePlanet, 10000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    onPlanetLoaded = (planet) => {
        this.setState({
            planet,
            loaded: true,
        });
    };

    onError = (err) => {
        this.setState({
            error: true,
            loaded: false,
        });
    };

    updatePlanet = () => {
        const id = Math.floor(Math.random() * 20) + 1;
        this.swapiService
            .getPlanet(id)
            .then(this.onPlanetLoaded)
            .catch(this.onError);
    };

    render() {
        const { planet, loaded, error } = this.state;

        const errorIndicator = error ? <ErrorIndicator /> : null;
        const spinner = !loaded && !error ? <Spinner /> : null;
        const content =
            loaded && !error ? <PlanetView planet={planet} /> : null;

        return (
            <div className="random-planet row justify-content-center align-items-center">
                {errorIndicator}
                {spinner}
                {content}
            </div>
        );
    }
}

const PlanetView = ({ planet }) => (
    <>
        <div className="col-md-4">
            <img
                className="random-planet__img"
                src={planet.img}
                alt="random planet"
            />
        </div>
        <div className="col-md-4">
            <h4>{planet.name}</h4>
            <DetailsList list={planet.details} />
        </div>
    </>
);
