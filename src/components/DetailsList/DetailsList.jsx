import React from 'react';

import './DetailsList.css';
import ListLine from '../ListLine';

const DetailsList = ({ list }) => {
    if (list === undefined) {
        return null;
    }

    const details = list.map(({ name, value }, idx) => (
        <ListLine label={name} value={value} key={idx} />
    ));

    return <ul className="list-group">{details}</ul>;
};

export default DetailsList;
