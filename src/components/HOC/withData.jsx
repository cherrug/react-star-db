import React from 'react';
import SwapiService from '../../services/SwapiService';
import Spinner from '../Spinner';
import ErrorIndicator from '../ErrorIndicator';

const withData = (View, getData) =>
    class extends React.Component {
        swapi = new SwapiService();

        state = {
            data: null,
            error: false,
        };

        componentDidMount() {
            this.updateList();
        }

        updateList() {
            const { itemId } = this.props;
            const req = itemId ? getData(itemId) : getData();

            req.then((data) => {
                this.setState({ data });
            }).catch(this.onError);
        }

        onError = () => {
            this.setState({
                error: true,
            });
        };

        render() {
            const { data, error } = this.state;

            if (!data) {
                return <Spinner />;
            }

            if (error) {
                return <ErrorIndicator />;
            }

            return <View {...this.props} data={data} />;
        }
    };

export default withData;
