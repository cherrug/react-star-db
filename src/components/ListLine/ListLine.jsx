import React from 'react';

const ListLine = ({ label, value }) => (
    <li className="list-group-item d-flex justify-content-between align-items-center">
        <span>{label}</span>
        <span className="ml-1">{value}</span>
    </li>
);

export default ListLine;
