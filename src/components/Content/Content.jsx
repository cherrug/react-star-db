import React from 'react';

import './Content.css';
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';

const Content = ({ left, right }) => (
    <div className="row">
        <ErrorBoundary>
            <div className="col-md-6 pl-0">{left}</div>
        </ErrorBoundary>
        <ErrorBoundary>
            <div className="col-md-6 pr-0">{right}</div>
        </ErrorBoundary>
    </div>
);

export default Content;
