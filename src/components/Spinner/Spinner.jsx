import React from 'react';

import './Spinner.css';

const Spinner = () => (
    <div className="d-flex justify-content-center align-items-center">
        <div className="spinner spinner-border text-secondary">
            <span className="sr-only">Loading...</span>
        </div>
    </div>
);

export default Spinner;
