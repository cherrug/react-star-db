export default class SwapiService {
    constructor() {
        this._apiBase = 'https://swapi.co/api';
    }

    getResourse = async (u) => {
        const url = `${this._apiBase}${u}`;
        const res = await fetch(url);

        if (!res.ok) {
            throw new Error(`Could not fetch ${url}, status ${res.status}`);
        }

        return await res.json();
    };

    getAllPeople = async () => {
        const res = await this.getResourse('/people/');
        return res.results.map(this._transformPerson);
    };

    getPerson = async (id) => {
        const person = await this.getResourse(`/people/${id}`);
        return this._transformPerson(person);
    };

    getAllPlanets = async () => {
        const res = await this.getResourse('/planets/');
        return res.results.map(this._transformPlanet);
    };

    getPlanet = async (id) => {
        const planet = await this.getResourse(`/planets/${id}`);
        return this._transformPlanet(planet);
    };

    getAllStarships = async () => {
        const res = await this.getResourse('/starships/');
        return res.results.map(this._transformStarship);
    };

    getStarship = async (id) => {
        const ship = await this.getResourse(`/starships/${id}`);
        return this._transformStarship(ship);
    };

    _getId = (url) => {
        const regexId = /.*\/(\d+)\/$/;
        return url.match(regexId)[1];
    };

    _transformPlanet = (planet) => {
        const id = this._getId(planet.url);
        return {
            id,
            name: planet.name,
            img: `https://starwars-visualguide.com/assets/img/planets/${id}.jpg`,
            details: [
                { name: 'Population', value: planet.population },
                { name: 'Rotation Period', value: planet.rotation_period },
                { name: 'Climate', value: planet.climate },
                { name: 'Gravity', value: planet.gravity },
            ],
        };
    };

    _transformPerson = (person) => {
        const id = this._getId(person.url);
        return {
            id,
            name: person.name,
            img: `https://starwars-visualguide.com/assets/img/characters/${id}.jpg`,
            details: [
                { name: 'Birth Year', value: person.birth_year },
                { name: 'Height', value: person.height },
                { name: 'Mass', value: person.mass },
            ],
        };
    };

    _transformStarship = (ship) => {
        const id = this._getId(ship.url);
        return {
            id,
            name: ship.name,
            img: `https://starwars-visualguide.com/assets/img/starships/${id}.jpg`,
            details: [
                { name: 'Model', value: ship.model },
                { name: 'Length', value: ship.length },
                { name: 'Crew', value: ship.crew },
                { name: 'Class', value: ship.starship_class },
            ],
        };
    };
}
